# Flutter Firebase - The Full Course

Build a QuizApp from scratch with Flutter 1.5 & Firebase. 

[Get it on The App Store](https://itunes.apple.com/us/app/fireship/id1462592372?mt=8)

[Get it on Google Play](https://play.google.com/store/apps/details?id=io.fireship.quizapp)


<!-- # Branches

1. User Authentication
2. Firestore Service & State Management
3. Topics List and Quiz -->


